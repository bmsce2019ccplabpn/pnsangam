#include <stdio.h>
int main()
{
    int n,i=0,sml,lr,t,posA=1,posB=1;
    printf("Enter The Number Of Numbers:");
    scanf("%d",&n);
    int numb[n];
    for(i=0;i<n;i++)
    {
        printf("enter the %d number: ",i+1);
        scanf("%d",&numb[i]);
    }
    
    printf("list of the arrays\n");
    for(i=0;i<n;i++)
    {
        printf("%d number=%d\n",i+1,numb[i]);
    }
    
    sml=numb[0];
    for(i=0;i<n;i++)
    {
        if(numb[i]<sml)
            {  sml=numb[i];
               posA=i+1;   }
    }
    lr=numb[0];
    for(i=0;i<n;i++)
    {
        if(numb[i]>lr)
            {  lr=numb[i];
               posB=i+1;   }
    }
    
    printf("Smallest of all=%d and position=%d\n",sml,posA);
    printf("Largest of all=%d and position=%d\n",lr,posB);
    
    printf("\nAfter interchanging the numbers\n");
    printf("\nSmallest of all=%d and Position Is %d\n",sml,posB);
    printf("Largest of all=%d and Position Is %d\n",lr,posA);
    
    return 0;
}
